---
title: Virtualbox images resizing
author: Nicolas
date: '2017-12-18'
slug: virtualbox-images-resizing
categories:
  - FOSS
  - blogging
tags:
  - virtualbox
  - OSGeoLive
description: ''
featured: ''
featuredalt: ''
featuredpath: ''
linktitle: ''
---

I add to resize an OSGeoLive virtual machine because after a couple postgresql workshops with quite big datasets (less than 1 GB each but still), the default size was not enough.

So I followed the instructions from here (in French): http://blogf.leunens.fr/2015/1306/agrandir-la-taille-dun-disque-virtualbox.html

Please note that,by default, OSGeoLive cames into a vmdk file so you need to convert it to a VDI to increase the volume size. If you already use a VDI file, not need to do that step.

```{bash}
# get into the VM storage folder
cd /media/data/MachinesVirtuelles/OSGEOLIVE\ workshop/

# check for the vmdk file
ls

# clone vmdk image into a new vdi one
VBoxManage clonehd --format VDI OSGEOLIVE\ workshop-disk001.vmdk OSGEOLIVE_workshop.vdi
## STOP HERE AND CHECK THAT THE NEW ONE WORKS

# Resze the new image
VBoxManage modifyhd  OSGEOLIVE_workshop.vdi --resize 40960
```

At this point you have a new image with a 40GB storage, but the partitions inside are still the same size, you need to resize it.

To do that, I used a Gparted live cd from [gparted.org](https://gparted.org/livecd.php).

![New drives](/img/2017/12/resize_VM/new_devices.png)

Then boot on the GParted LiveCD and follow instructions.

I deleted the swap partition because system already had 8 GB of RAM and I'm on a SSD so I try to avoid swapiness.

The final result is a 40 GB ext4 partition.

![Big partition](/img/2017/12/resize_VM/final_size.png)

When finished, stop the GParted liveCD, change VM boot options (Hard Drive before Optical) and check that the Live CD is no more in the storage devices.

Boot the machine with the new storage device. It will take some time for the system to check the new filesystem but it should be ok.

![Check new size](/img/2017/12/resize_VM/view_new_size_in_VM.png)

As you can see / as now 18G available on 40G. Mission completed !

