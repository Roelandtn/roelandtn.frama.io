---
title: September 2018 datascience goals
author: Nicolas
date: '2018-03-31'
slug: september-2018-datascience-goals
categories:
  - DataScience
  - life
tags:
  - DataScience
  - Python
  - R
---

Last updated: `r Sys.Date()`

I know, I know, my last post was 2 months ago. I'm not very steady but lots have been done on other things ([FOSS4G-fr 2018  program](https://www.osgeo.asso.fr/foss4gfr-2018/programme.html), a bread recipe that I can manage, writing a python course from scratch, that kind of things).

Next step in my life will be in September at the end of my Master degree. I don't know what I will do at that time but I know what I want to do. That's a start.

During that Master degree, I learned a lot especially in programmation (Python, PHP, Java), databases (PostgreSQL/PostGIS, MySQL), datascience (R, SAS, SPAD) and data manipulation (FME, arcpy). 

That's a lot. Really, I don't master any of those but I feel confident with some of them. I decided to focus on only 3:

* Python3 (obviously as I teach it)
* R (there is a lot to learn, to do and the community is great)
* PostgreSQL/PostGIS (I like it and there is a lot of great resources available)

Despite the fact it is a Master degree in Geography, I find it pretty well suited for someone who want to embrace a datascience career. Which I plan to do. 

I order to that, this post is a list of things to read, meetup to attend, MOOCs to follow and to do.
As I'm French, some of those resources are in French, and sometimes, it doesn't make it easier to understand for me.

As I'll tried to update this list as regularly as possible and I hope will think to make a review of done things in October. It will help me see my progess. (I hope so)

# On the list
## School stuff

School is over for me ! (At least for the moment).
May, June and early July were pretty intense, but I should get my Master Degree in September 2018 ! Yay !

## Package

* [covRlettR, a cover letter generator](https://github.com/Bakaniko/covRlettR):
    + Markdownize it:
        - letter
        - resumé

## Books to read

* [Stats faciles avec R - Guillaume Broc and al.](http://www.deboecksuperieur.com/ouvrage/9782807304789-stats-faciles-avec-r): 33 chapters
* [Think like a Data Scientist - Brian Godsey](https://www.manning.com/books/think-like-a-data-scientist): 13 chapters
* [Rspatial - 
Spatial Data Analysis and Modeling with R](http://www.rspatial.org)
* [R for Datascience - Hadley Wickam](http://r4ds.had.co.nz): 30 chapters
* [Geocomputation with R - 
Robin Lovelace and al.](https://geocompr.robinlovelace.net): 13 chapters
* [Advanced R Course - Florian Privé](https://privefl.github.io/advr38book/index.html)
* [PosgreSQL handouts -  Dalibo](https://www.dalibo.com/formations)
* [Scipy Lecture Notes](http://gael-varoquaux.info/scipy-lecture-notes/index.html)
* [YaRrr! The Pirate’s Guide to R](https://bookdown.org/ndphillips/YaRrr/)
* [PostGIS in action](https://www.manning.com/books/postgis-in-action)
* [A Gentle Guide to the Grammar of Graphics with ggplot2](https://www.garrickadenbuie.com/talk/trug-ggplot2/)

* [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown)


## MOOCs to follow
* Berkeley Datascience course: 
    + https://www.edx.org/professional-certificate/berkeleyx-foundations-of-data-science#courses
    + http://news.berkeley.edu/2018/03/29/berkeley-offers-its-fastest-growing-course-data-science-online-for-free/
* [Exploratory Multivariate Data Analysis ](https://www.fun-mooc.fr/courses/course-v1:agrocampusouest+40001S04EN+session04/about)
* [Python 3 : des fondamentaux aux concepts avancés du langage - FUN MOOC ](https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session01/about)

## Post to write

* Mexico Project
    - Problematic, data source, and variable selection
    - Data management and handling
    - Visualisation of the variables
    - Principal components analysis
    - Hierachical clustering
    - Correspondence analysis
    - Results and conclusion
    - Tools

* Meetup notes
    + Données géographiques et cartographie sous R
    + Vim à vie #1 : Pourquoi et comment ?
    + Typography and R 

# Achievements
## Presentations
* [Passage en Seine - FOSS and Commons convention](https://passageenseine.fr)
    + [What is GIS ? (in French) - recorded](http://data.passageenseine.org/2018/bakaniko_kesaco-geomatique-libre.webm)

## School stuff
* Master thesis (late June 2018) + defense (early July 2018) => done ! 
* GIS 1st year Master degree Python3 course:
    + lesson given
          - 1 built-in types (part 1),
          - 2 built-in types (part 2),
          - 3 Comparisons and tests, 
          - 4 Loops and iterations, 
          - 5 functions and shared referencees, 
          - 6 functions and modules,
          - 7 text file manipulation
          - 8 Classes and methodes
          - 9 Inheritance, Method overriding
          - 10 List Comprehensions, lambda functions
          - 11 Error handling
          - 12 [PyQGIS 101: Introduction to QGIS Python programming for non-programmers](https://anitagraser.com/pyqgis-101-introduction-to-qgis-python-programming-for-non-programmers/) 
          - 13 Exam
          - 14 [Scipy 2017 Python pandas tutorial](https://github.com/chendaniely/scipy-2017-tutorial-pandas)
    + 2018 - 2019 : 2nd semester lesson to prepare 

## Package
* covRlettR, a cover letter generator:
    + Create a simple package that integrate TinyTeX and Moderncv LaTeX template, then publish => [done](https://github.com/Bakaniko/covRlettR)
    
## Red books
* [Think like a Data Scientist - Brian Godsey](https://www.manning.com/books/think-like-a-data-scientist): 3/13 chapters

## Completed MOOCs

## Written posts

- Problematic, data source, and variable selection

## Attended Meetups

* PyData Paris
    + [January 2018 Meetup](https://www.meetup.com/fr-FR/PyData-Paris/events/246516937/) 
* RLadies
    + [Données géographiques et cartographie sous R](https://www.meetup.com/fr-FR/rladies-paris/events/244054797/)
    + [Mixed effects regression models](https://www.meetup.com/fr-FR/rladies-paris/events/252021214/)
    
* Vim - SilexLabs
    + [Vim à vie #1 : Pourquoi et comment ?](https://www.meetup.com/fr-FR/Silex-Labs-Aperopensource/events/248642423/)

* RAddicts Paris
    + R Addicts x EDF
    + [R Addicts x Malt](https://www.meetup.com/fr-FR/rparis/events/252405360/?comment_table_id=493070115&comment_table_name=event_comment)
* 
