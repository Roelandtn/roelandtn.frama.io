---
title: First post !
author: Nicolas
date: '2017-08-20'
slug: first-post
categories:
  - blogging
tags:
  - blog
description: ''
featured: ''
featuredalt: ''
featuredpath: ''
linktitle: ''
---

First of all, welcome to this site !

As the about says, this blog purpose is to talking about OpenGIS, Python, R, data nalysis and stuff like that.

I will be probably publishing learning notebooks as i do them to improve my knowledge.

When possible, I'll publish original stuff, depends on current affairs.

Cheers !

Nicolas

