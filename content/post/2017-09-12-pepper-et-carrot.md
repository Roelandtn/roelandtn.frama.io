---
title: Pepper et Carrot
author: Nicolas
date: '2017-09-12'
slug: pepper-et-carrot
categories:
  - FOSS
  - life
tags:
  - FOSS
  - blog
  - OpenSource
description: "Meet an artist and talk about FOSS: check !"
featured: "david_revoy_2017.jpg"
featuredalt: "Me and David Revoy at A Livr\'Ouvert library"
featuredpath: 'date'
linktitle: ''
---

Today I went to the library [A livr'Ouvert](https://www.alivrouvert.fr/) because there was a sign session with [David Revoy](http://davidrevoy.com).

It is an artist that I really like for two reasons:

1. his stories are fun and he has really nice drawing;
2. he used free and opensource software to produce his art and his artwork is licenced with a Creative Commons Attribution 4.0 licence;
3. People who like Ubuntu MATE can't be bad people (well, I'm not really objective).


He also provides knowledge on his art and tools by making videos, blog posts, Krita toolbox, etc. He is also a [contributor](https://github.com/Deevad) to the [Krita project](https://krita.org/).

His work is the living proof that you can produce high quality material with open source software, you can live from it (visit his [patreon page](https://www.patreon.com/davidrevoy)) and still be edited by a [major book compagny](http://www.glenatbd.com/bd/pepper-et-carrot-tome-1-9782344017258.htm).

David Revoy is also a geek and it shows in his drawing and subject.

When signing the books (with a big pencil drawing in front page), he also took the time to chat with the people and to exchange with them. I spent a great time chatting with him and my brother about linux distos and free software.

![Dedicace](/img/2017/09/dedicace_david_revoy_12_09_2017.jpg) 

We spend a really nice moment with him and  and the [A livr'Ouvert](https://www.alivrouvert.fr/) staff (<3 Bookynette). And I hope we will meet them again, for the third tome maybe ? Stay tuned !

