---
title: Pandoc and Xelatex in docker
author: Nicolas
date: '2017-08-26'
slug: august2017
categories:
  - Writing
  - Projects
tags:
  - pandoc
  - latex
  - xelatex
  - docker
description: ''
featured: ''
featuredalt: ''
featuredpath: ''
linktitle: ''
draft: FALSE
---

This side project attends to use a docker image for pandoc and LaTex related stuff.
TexLive is not very easy to maintain even on Ubuntu. 

Especially to update, since Texlive freeze every year and Ubuntu LTS versions have some retard (e.g on Ubuntu 16.04, source packages are about TexLive 2015, but TexLive servers provides 2017 packages). Plus, TexLive is really big and take a huge place in the root filesytem. Docker archives can be stored in the home folder (bigger in my current configuration) or in another partition without problems. They also can be moved easily.  

So I tell myself, why not create a special docker image, with fresh softwares (TexLive 2017, Pandoc 1.19.2) 

To do :
- make a docker image
- config .bashrc to use docker image
- tweaking the image (if needed)
- add a `Projects` categorie on this website
- test with Bookdown and Blogdown

