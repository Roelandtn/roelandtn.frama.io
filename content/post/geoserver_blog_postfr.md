
### Installer Geoserver sur CentOS



* Plusieurs possibilités:
	- installer le serveur applicatif Tomcat et l'utiliser pour déployer Tomcat
	- installer GeoServer

La première solution a été retenue.

**Machine de départ**: VM avec CentOS

#### Vérifier si tomcat est fonctionnel
```bash
# Voir la version installée (ou non)
yum info tomcat

# si paquets disponibles : disponibles dans les dépots mais pas installés
# sudo yum install tomcat
# sudo yum install tomcat-webapps tomcat-admin-webapps

# si paquets installés: les paquets ont été installés
# version installée (au 11/09/2017): 7.0.69


#Voir si le tomcat est lancé
sudo service tomcat status
# running, le serveur est fonctionne
# enable: le démarrage automatique au redémarage du serveur est activé

# si le service n'est pas lancé
sudo service tomcat start
```
##### Lancer le service tomcat au démarrage de la machine
```bash
sudo systemctl enable tomcat.service
```


#### Copier le fichier goeserver.war

```bash
cd ~
sudo mv geoserver.war /var/lib/tomcat/webapps
```

Le service devrait être opérationnel quelques minutes plus tard.

#### Vérifier les ports ouverts sur la machine
```bash
sudo cat /etc/sysconfig/iptables
# les ports 8080 et 8443 devraient être ouverts:
-A INPUT -m state -state NEW -m tcp -p tcp -dport 8080 -j ACCEPT
-A INPUT -m state -state NEW -m tcp -p tcp -dport 8443 -j ACCEPT

```

#### Vérifier l'accessibilité du service

Se rendre avec un navigateur à la page suivante:
`http://<mon beau serveur>:8080/geoserver/web/`

La page suivante devrait s'afficher

![Acceuil de GeoServer](images/acceuil_geoserver.JPG)

ou utiliser la commande suivante:

```bash
curl http://<mon beau serveur>:8080/geoserver/web/
```
### Accéder à l'interface d'administration
Il faut se logguer avec les identifiants suivants:
- login : admin
- mdp: geoserver

### Déployer un raster
Se connecter sur la page d'administration avec le login `admin` et le mot de passe `geoserver`. **A CHANGER**

[Blog: Débuter avec geoserver: ](https://www.sigterritoires.fr/index.php/debuter-avec-geoserver/)

https://www.sigterritoires.fr/index.php/debuter-avec-geoserver/

#### Formats supportés

GeoServer supporte nativement les formats suivants:
    - GeoTIFF
    - GTOPO30
    - WorldImage
    - ImageMosaic

D'autres formats sont utilisables en ajoutant des extensions.

**Attention** les numéros de version des extensions doivent correspondre au numéro de version de l'instance GeoServer.

    - ArcGrid
    - GDAL Image Formats
    - Oracle Georaster
    - Postgis Raster
    - ImagePyramid
    - Image Mosaic JDBC
    - Custom JDBC Access for image data

##### Installer le plugin GDAL

Source : http://docs.geoserver.org/stable/en/user/data/raster/gdal.html


Pour ouvrir certains formats de fichier, GeoServer utilise le plugin GDAL.
Il peut être téléchargé à cette adresse: http://geoserver.org/release/stable/

Il faut faire attention au numéro de version, par exemple, pour GeoServer *2.11.2*, il faut télécharger l'archive `geoserver-2.11.2-gdal-plugin.zip`

Une fois l'archive téléchargée il faut la copier dans le répertoire et la décompresser dans le dossier `WEB-INF/lib`

```bash
# l'archive a été copiée dans /home/admin/geoserver

mkdir /home/admin/geoserver/gdal-plugin
cp geoserver-2.11.2-gdal-plugin.zip gdal-plugin/
cd gdal-plugin
unzip geoserver-2.11.2-gdal-plugin.zip
rm geoserver-2.11.2-gdal-plugin.zip
sudo cp * /var/lib/tomcat/webapps/geoserver/WEB-INF/lib/

# Cleaning
cd .. && rm -Rf gdal-plugin/
```

##### Installer les bibliothèques natives de GDAL

le plugin GDAL utilise pour son fonctionnement les composants de la bibliothèque
GDAL (et donc lire les fichiers VRT). Il faut donc les installer.

Il faut télécharger et installer 2 archives:
- [les définitions CRS](http://demo.geo-solutions.it/share/github/imageio-ext/releases/1.1.X/1.1.16/native/gdal/gdal-data.zip) :  
	- http://demo.geo-solutions.it/share/github/imageio-ext/releases/1.1.X/1.1.16/native/gdal/gdal-data.zip
- [les librairies natives](http://demo.geo-solutions.it/share/github/imageio-ext/releases/1.1.X/1.1.16/native/gdal/linux/): 	 
	- http://demo.geo-solutions.it/share/github/imageio-ext/releases/1.1.X/1.1.16/native/gdal/linux/gdal192-CentOS5.8-gcc4.1.2-x86_64.tar.gz


Le paquet [gdal-java](https://centos.pkgs.org/7/epel-x86_64/gdal-java-1.11.4-1.el7.x86_64.rpm.html) procure un certain nombre des fichiers installés ici, mais pas tous. Il faudrait le tester pour voir si i s'agit d'une alternative plus simple.

###### Installer les CRS

Il faut copier l'archive **gdal-data.zip** dans un répertoire approprié et la décompresser et créer la variable d'environnement `GDAL_DATA` pointant vers ce répertoire.

Documentation: http://trac.osgeo.org/gdal/wiki/FAQInstallationAndBuilding#WhatisGDAL_DATAenvironmentvariable


```bash
# création du répertoire de destination
sudo mkdir /usr/local/share/gdal

# Décompression de l'archive dans le répertoire de destination
sudo unzip gdal-data.zip -d /usr/local/share/gdal

# Création de la variable d'environnement
export GDAL_DATA=/usr/local/share/gdal

# Tester l'installation de gdal
gdalinfo --formats # doit retourner la liste des formats gérés
```

![Commande gdalinfo](images/geoserver_gdalinfo.JPG)

###### Installer les bibliothèques natives

Documentation: https://github.com/georchestra/georchestra/blob/master/geoserver/NATIVE_LIBS.md

```bash
# création du répertoire de destination
sudo mkdir /usr/local/share/gdal-geoserver

# Décompression de l'archive dans le répertoire de destination
sudo tar xzvf gdal192-CentOS5.8-gcc4.1.2-x86_64.tar.gz \
 -C /usr/local/share/gdal-geoserver

# Création de la variable d'environnement
export LD_LIBRARY_PATH=/usr/local/share/gdal-geoserver
```

###### Java bindings

Il faut aussi copier le fichier `imageio-ext-gdal-bindings-1.9.2.jar`
dans le répertoire `lib/` de la webapp **geoserver**

```bash
sudo cp /usr/share/gdal-geoserver/javainfo/imageio-ext-gdal-bindings-1.9.2.jar \
 /var/lib/tomcat/webapps/geoserver/WEB-INF/lib/

# changement des propriétaires des fichiers
sudo chown tomcat:tomcat /usr/share/tomcat/webapps/geoserver/WEB-INF/lib/*
```


##### Ajouter les variables d'environnement à la configuration de Tomcat
###### Ajouter LD_LIBRARY_PATH
Cette variable est essentielle à GeoServer pour utiliser GDAL.

Ajouter la ligne suivante à la fin de `/etc/tomcat/tomcat.conf`

```bash
LD_LIBRARY_PATH=/usr/local/share/gdal-geoserver/
```

###### Optionnel / alternative / Complément

Les lignes ci-dessous sont à ajouter dans `/usr/share/tomcat/bin/setenv.sh`
```bash
# Création de la variable d'environnement
### les 3 options d'export peuvent être rajoutées à ~/.bashrc pour l'utilisateur courant pour tester GDAL


export GDAL_DATA=/usr/local/share/gdal
export PATH=/usr/local/share/gdal-geoserver:$PATH
export LD_LIBRARY_PATH=/lib:/usr/local/share/gdal-geoserver:/usr/local/share/gdal-geoserver/javainfo:$LD_LIBRARY_PATH

CATALINA_OPTS="[...]
 -DGDAL_DATA=$GDAL_DATA \
 -Djava.library.path=$LD_LIBRARY_PATH \
  [...]
"
```

##### Redémarrer le service Tomcat

```bash
# Redémarrage
sudo systemctl restart tomcat.service
sudo systemctl status tomcat.service
```

#### stockage des données
Les données servies sont stockées dans le répertoire `/var/lib/tomcat/webapps/geoserver/data/data/`

#### Créer un espace de travail

Les fichiers au sein du service sont ordonnés sous forme d'espaces de travail (*Workspace*) que l'on peut voir comme des projets / sous-projets.

Chaque espace de travail pourra contenir plusieurs entrepôts de données, qui pourront être des fichiers shapefiles, GeoTIFF, etc.

A noter qu'une base Postgis correspond à un espace de travail.

- Nom du nouvel espace de travail: `ORTHO_2015`
- URI: `http://<mon beau serveur>:8080/ORTHO_2015`

L'URI (Uniform Resource Identifier) est habituellement une URL associé avec le projet avec un identifiant terminal rappelant l'espace de travail.L'URI ne nécessite pas de résoudre une adresse réelle.

![Création d'un nouvel espace de travail](images/geoserver_nouvel_espace_de_travail.JPG)

#### Visualiser les données
Les données sont visibles en ligne depuis l'interface d'administration:
`Données` -> `Prévisualisation de la couche`, choisir une couche et cliquer sur `OpenLayers`.

Le flux est disponible depuis l'adresse suivante:

`http://<mon beau serveur>:8080/geoserver/ORTHO_2015/wms?`

`ORTHO_2015` correspondant au nom de l'espace de travail défini.

#### Copier des données externes
Copier les fichiers à l'aide WinSCP dans le dossier `/home/admin/geoserver/ORTHO_2015`

```bash
sudo mkdir /var/lib/tomcat/webapps/geoserver/data/data/ORTHO_2015

sudo mv /home/admin/geoserver/ORTHO_2015/* \
 	/var/lib/tomcat/webapps/geoserver/data/data/ORTHO_2015/
```

#### Ajouter un entrepôt de données

Pour GeoServer, un entrepôt de données est un fichier contenant des informations.
Ainsi, chaque raster est un entrepôt de données, chaque shapefile est un entrepôt de données.

![Créer un nouvel entrepôt de données](images/geoserver_ajouter_entrepot.JPG)

![Ajouter le VRT 1/2](images/geoserver_ajouter_VRT.JPG)

![Ajouter le VRT 2/2](images/geoserver_ajouter_VRT2.JPG)

![Publier la ressource](images/geoserver_publier_ressource.JPG)

![Editer la ressource](images/geoserver_editer_ressource.JPG)

![Voir les couches](images/geoserver_couches.JPG)

### Créer un VRT
#### Avec QGIS
Dans QGIS: `Raster` -> `Divers` -> `Constuire un Raster Virtuel (Catalogue VRT)...`

2 possibilités:
- soit ouvrir tous les fichiers dans QGIS et cocher la case `Utiliser les rasters visibles comme entrée`
- soit `Sélectionner un répertoire plutôt qu'un fichier` ce qui chargera tous les fichiers rasters présents dans le dossier désigné

Il faut renseigner le nom de fichier en sorie, il est possible de spécifier le SCR cible (ici, `EPSG:2154`).

![Créer un VRT dans QGIS](images/qgis_creation_VRT.JPG)


#### Chemin de fichier

Lors de la création du *VRT* par **QGIS**, les chemins vers les fichiers sont
écrits en dur dans le code XML. Ainsi tous les fichiers comportent le chemin vers
`D:\Projets\GEOSERVER\VRT_QGIS\ORTHO 2015\`. Il faut remplacer ce chemin par
`/var/lib/tomcat/webapps/geoserver/data/data/ORTHO_2015/` à l'aide d'un éditeur de texte avant de copier les fichiers sur le serveur.


### Flux wms
#### visualisation depuis un navigateur
Le flux est alors disponible et visible à une adresse de ce type:
http://<mon beau serveur>:8080/geoserver/test/wms?service=WMS&version=1.1.0&request=GetMap&layers=test:Ortho2015_test_var_lib&styles=&bbox=585000.0,6865000.0,735000.0,6910000.0&width=768&height=330&srs=EPSG:2154&format=application/openlayers



#### Ajouter un flux dans Portal for ArcGIS

Documentation ESRI: http://server.arcgis.com/fr/portal/latest/use/add-layers.htm

Portal for ArcGIS est assez contraignant car il convertit toutes les urls en HTTPS.

##### Contrôle de l'ouverture des ports
Il faut donc vérifier que le port **8443** est ouvert dans iptables

```bash
sudo cat /etc/sysconfig/iptables

# retour attendu
-A INPUT -m state -state NEW -m tcp -p tcp -dport 8080 -j ACCEPT
-A INPUT -m state -state NEW -m tcp -p tcp -dport 8443 -j ACCEPT

```

##### Importer le certificat

```bash
keytool -importkeystore -deststorepass {PASSWORD} -destkeypass {PASSWORD}  -destkeystore /usr/share/tomcat/keystore -srckeystore {CERT_PATH}/<mon_certificat>.pfx -srcstoretype PKCS12 -srcstorepass {PASSWORD}
```

###### Paramètres
* `{PASSWORD}` : mot de passe utiliser pour chiffrer le keystore, qui sera à configurer dans tomcat ultérieurement. Il doit être identique à chaque fois
* `{CERT_PATH}` : chemin vers le fichier *<mon_certificat>.pfx* souvent`.`

```bash
ls /usr/share/tomcat/
```

![Contrôle de la présence du keystore](images/tomcat_keystore.png)

##### Activer le HTTPS dans Tomcat
###### Server.xml
Tomcat va chercher ses paramètres serveurs dans le fichiers `serveur.xml`

```bash
sudo vim /usr/share/tomcat/server.xml
```

Il faut que décommenter et paramétrer le connecteur **8443**  dans la balise `<Service>`:
```xml
<Connector port="8443" protocol="org.apache.coyote.http11.Http11Protocol"
         maxThreads="150" SSLEnabled="true" scheme="https" secure="true"
         clientAuth="false" sslProtocol="TLS"
         keystoreFile="/usr/share/tomcat/keystore" keystorePass="{PASSWORD}"/>
```

> **Attention**: le `{PASSWORD}` doit être le même utilisé pour le *keystore* créé précédemment.

##### Redémmarrer le service Tomcat
Pour la prise en compte des nouveaux paramètres, le service Tomcat doit être redémmarré.


```bash
sudo systemctl restart tomcat.service
```
##### Ajouter le flux dans le portal

Depuis l'interface de **WebApp Builder**, dans l'outil `Charger des données`, sélectionner *URL*, puis le type  *Service Web OGC WMS*

Dans le champ *URL* saisir l'URL suivante pour charger toutes les couches servies par le serveur:

> https://<mon beau serveur>:8443/geoserver/wms?service=WMS&version=1.3.0&request=GetCapabilities

![Ajout d'un flux OGC WMS](images/portal_ajout_flux_OGC_WMS.JPG)

Cliquer sur `Ajouter`.

Dans la liste des couches, les couches fournies par le serveur apparaissent.

![Résultat final - Flux WMS chargé dans Portal](images/geoserver_wms_portal.JPG)

### Impression 300dpi

A partir de Portal, il est possible à l'aide du widget **Imprimer** d'exporter la carte sous différents formats:

- EPS
- GIF
- JPEG
- PDF
- PNG8
- PNG32
- SVG
- SVGZ

Toutefois, l'impression en haute résolution (> 96 dpi) au format **pdf** génére des artefacts (lignes blanches) avec Firefox (pdf.js). C'est pourquoi il est recommandé de privilégier d'imprimer à partir d'Acrobat Reader.

![Résultat final - Flux WMS chargé dans Portal](images/portal_impression_300dpi.JPG)
