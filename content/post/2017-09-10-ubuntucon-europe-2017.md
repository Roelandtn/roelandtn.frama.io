---
title: UbuntuCon Europe 2017
author: Nicolas
date: '2017-09-10'
slug: ubuntucon-europe-2017
categories:
  - FOSS
  - life
tags:
  - Ubuntu
  - UPP
  - Cooking
  - FOSS
description: 'Weekend at UbuntuCon EUrope 2017 in Paris'
featured: 'ubucon_2017.jpeg'
featuredalt: ''
featuredpath: 'date'
linktitle: ''
---

I just came home from this [UbuntuCon Europe 2017](https://ubucon.paris), who was held in *La Cité des Sciences et de l'Industrie*, in Paris. SO I was able to attend it, although I followed one talk, about privacy on internet.

I met great people, starting by my friend Marcos Costales, a spanish developper known for [uNav, THE gps application for Ubuntu Touch](https://uappexplorer.com/app/navigator.costales) or [Gufw, a minimalist firewall](https://gufw.org/).

I was able to meet [Martin Wimpress](http://wimpress.org/#page-top) (known for [Ubuntu MATE](https://ubuntu-mate.org/)) or Alan Pope

There was a concert too, by the [KICKBAN](http://kickban.fr/) geek rock band. That was very interesting to discover. A must-have background music in your openspace.

So, what did I do, if I didn't follow speechs or workshops ? I cooked. Yes, I cooked. For **UPPs** (which stands for *Ubuntu Party Parties*), lunchs are made for volunteers and speakers by volunteers.

It was my 5th UPP, and the forth time for me in the kitchen. I could be in the install room, or in the hall room, welcoming visitors and showcase the merits and virtue of Ubuntu and FOSS, but I prefer the kitchen.
There is several reasons:

- it is quiet
- so you can chat with people
- I feel really useful because kitchen staff lacked people to help organise it, so I felt immediatly useful.
- you do something pratical (cutting vegetables, making sandwiches, ...) and you can see your result
- and, the best is that people actually enjoy what you did ! And that's priceless.

For that, we work on the menu with *Karum*, president of the [French Ubuntu association](http://ubuntu-fr.org/association). We make the menues, do the food shopping (althought, this time iI wasn't free) and prepare the food with the volunteers (big thanks to René, Twopiradians, Genoubi, Bou, Anne, etc). This time we served 50 meals saturday and sunday.  All food is fresh and hand made (but we takes some shortcuts because some external constraints). 

I wasn't available the friday, but they served russian salad and russian cornet. Saturday we made taboulé with fish or chicken mix with guacamole *galettes*. Sunday, it was *salade piémontaise* with a salmon or cured ham with fresh cheese sauce wrap. We try to be creative.

Main point is, I don't use a computer, I don't code but I contribute. There is many ways to contribute to Free and Open Source Software, I find a pleasant one.