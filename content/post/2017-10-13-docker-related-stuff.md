---
title: Docker related stuff
author: Nicolas
date: '2017-10-13'
slug: docker-related-stuff
categories:
  - FOSS
tags:
  - docker
description: ''
featured: ''
featuredalt: ''
featuredpath: ''
linktitle: ''
---

## Installing 
### On Ubuntu 16.04
```bash
sudo apt install docker-ce
```

## Moving images and containers

More information there: 

- [linuxconfig.org/how-to-move-docker-s-default-var-lib-docker-to-another-directory-on-ubuntu-debian-linux](https://linuxconfig.org/how-to-move-docker-s-default-var-lib-docker-to-another-directory-on-ubuntu-debian-linux)

- [http://alexander.holbreich.org/moving-docker-images-different-partition/](http://alexander.holbreich.org/moving-docker-images-different-partition/)

```bash
#Saving /etc/fstab
sudo cp /etc/fstab /etc/fstab.$(date +%Y-%m-%d)  

## get partitions UUID
sudo blkid

## Open fstab
sudo pluma /etc/fstab
```

Add the following :
```
## Data partition
UUID=<UUID of the wanted partition>	/media/data	ext4 defaults	0	2
```


```bash
whereis docker
# docker: /usr/bin/docker /usr/lib/docker /etc/docker /usr/share/man/man1/docker.1.gz

sudo service docker stop  
sudo mkdir /media/data/docker
ls
#if existiong docker directory
sudo rm -Rf docker/

sudo mkdir docker
cd docker/
rsync -aqxP /var/lib/docker/ .
sudo service docker start
sudo service docker status
```
### Adding new destination to fstab 

```bash
sudo pluma /etc/fstab
```

```
## new Docker folder
/media/data/docker /var/lib/docker none bind 0 0

```
## Some interesting docker images

### QGIS 

https://hub.docker.com/r/kartoza/qgis-desktop/

```bash
# for the latest
docker pull kartoza/qgis-desktop

# for the 2.14 release
docker pull kartoza/qgis-desktop:2.14

```
### PostGIS

https://hub.docker.com/r/kartoza/postgis/

```bash
# for the latest version
docker pull kartoza/postgis
```


## Using dockerized xelatex and pandoc
### Get the image
Used image: https://hub.docker.com/r/bakaniko/pandoc-latex/

```bash
docker pull bakaniko/pandoc-latex:0.1
```

### bashrc aliases
Add those lines to ~/.bash.rc

For pandoc:

> alias pandoc='docker run --rm -i -t -v $PWD:/data --user="$(id -u):$(id -g)" bakaniko/pandoc-latex:0.1 pandoc'

For Xetex/Xelatex:

> alias xetex='docker run --rm -i -t -v $PWD:/data --user="$(id -u):$(id -g)" bakaniko/pandoc-latex:0.1 xetex'

> alias xelatex='docker run --rm -i -t -v $PWD:/data --user="$(id -u):$(id -g)" bakaniko/pandoc-latex:0.1 xelatex'

## Tips

### bashrc aliases
Add those lines to ~/.bash.rc

To list all images on disk:

>   alias di="docker images"

To clean all orphaned images:

> function docker-clean(){
	    docker rmi -f $(docker images -q -a -f dangling=true)
  }



## Issues
I had some issues with docker (probably messed up with some configuration files) so I have to start the docker daemon manually.

```bash
sudo dockerd -s overlay
```

I solved it by removing the ~/.docker/docker folder and rsync again the folder with `/var/lib/docker/`.

EDIT: 

It is not recommended to use the file `/lib/systemd/system/docker.service`

Because it changes at each new version. If copied in `/etc/systemd/system`, they will be overloaded.


