---
title: OSGeo-Live Project
author: Nicolas
date: '2017-08-24'
slug: osgeoliveproject
categories:
  - OSGeo
  - Projects
tags:
  - osgeo
  - osgeo-live
description: ''
featured: ''
featuredalt: ''
featuredpath: ''
linktitle: ''
draft: FALSE
---

OSGeo-Live is a self-containing ISO with around 50 [FOSS](link to wikipedia) dedicated to GIS. A virtual macine archive is also provided.

I join the project in November (or early December) 2015, as I was already a librist when I was a became a GIS technician.
As I started a Bachelor degree in GIS, I need GIS tools to do assingnements, and they often needed FOSS like QGIS and/or PostgreSQL/PostGIS, or a webserver.

As I only have a laptop, I didn't want to have services running in the background just waiting to serve me data twice a week.

So I wanted to build a virtual machine, that I'll be running when needed. First my PostGIS installations were not very good, I had several troubles with other softwares. Then I encounter the OSGeo-Live project which provided a virtual machine with every thing I needed and more stuff to try out.

The documentation was very poor for the French language, so I joined as a translator. In that way, I hope to gain technical vocabulary in both languages ! 


As a student, I didn't have a lot of time but I tried to help as much as I can. The hardest was certaintly using *git*. This software is really useful but also a nigthmare to understand. It took me at least 6 months and a lot of frustrations to get used to the workflow. For that, a good GUI was necessary. I don't use it for commons stuff, but it still helps me to visualise and move between branches.

Now I can help newcomers who come to the project.

I did some French translations, as I check overviews and quikcstarts. Mostly for QGIS, PostGIS, R and GRASS, looking for typos and outdated instructions. I also do some artworks (10.5 and 11.0 wallpapers).

Since summer 2017, I work with Vicky Vergara on the documetation building process and the moving to the Transifex plateforme.
Since it is a big project, I'll write a special blog post about it. Vicky is a wonderful person, very energic and very **pédagogue**. She has a great knowledge in many areas and is really willing to share it.

My work in OSGeo-Live is not very technical but I can meet lots of people from various project and learn lots of things. 