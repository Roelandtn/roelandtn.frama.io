---
title: "Seul sur Mars - "
author: Nicolas
date: '2020-05-30'
slug: the-martian-mooc-sf
categories:
  - writing
tags:
  - writing
  - sf
description: ''
featured: ''
featuredalt: ''
featuredpath: ''
linktitle: ''
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

This is a short essay I wrote in French for a [MOOC on Science Fiction](https://www.fun-mooc.fr/courses/course-v1:univartois+35002+session03/info).

I had great pleasure to write it so, I wanted to publish it here.

I used [zettlr](https://www.zettlr.com/), a free markdown editor with special features
to store and retrieve knowledge. Its also the best tool so far I used to handle bibliography,
its [integration with Zotero](https://docs.zettlr.com/en/academic/citations/) is marvelous.

Zettlr also provide [tools dedicated for academic workflow](https://docs.zettlr.com/en/#academic-tools) 
but I didn't had to use them in that project. Maybe I should have.

What is document about ? The subject of the essay is to analyse an SF oeuvre and 
its relation to Science. What are the *novums* introduce by the author ? What is 
corresponding to our current scientific knowledge. At least, it is how I understood it.

This essay will be evaluated by 5 peers and there is some constraints : 
- less then 5000 characters,
- 2 parts, one on science representations in the oeuvre and one in their role in the story,
- clear expression,
- use of knowledge on science and science fiction.

5000 characters is really short and I could have done more of course.
At this point, it is 4600 characters (3500 without the bibliography).
I'm quite happy with the bibliography and it usage.

I'm less happy with the conclusion. I delivered as it is presented here but I 
could have redacted it a bit more. I'm not sure that the 2 parts are clearly 
identified and that they are balanced but that, it is not me to judge.

# Seul sur Mars

Seul sur Mars (Weir 2014b) est un roman écrit entre 2009 et 2012 par Andy Weir. C’est un roman qualifié par The Internet Speculative Fiction Database (Al von Ruff and the ISFDB team n.d.) comme Science Fiction, Futur proche et Hard SF.

L’auteur, Andy Weir, se définit lui-même (Weir 2015) comme un space nerd passionné d’aérospatial, allant jusqu’à programmer un calculateur de trajectoires orbitales. Il détaille cela dans un article intitulé “How Science made me a writer” (Weir 2014a).

Le roman prend la forme d’un journal de bord rédigé par Mark Watney, astronaute spécialisé en biologie végétale, gestion des ressources hydrologiques, irrigation et agriculture durable. Et bricoleur de génie. Des qualités utiles quand on se retrouve seul sur une planète à l’environnement hostile, après que les autres membres de la mission Arès 3 aient dû évacués en urgence, croyant leur camarade décédé. Le protagoniste devra donc faire avec ce qu’il a disposition et espérer tenir jusqu’à l’arrivée d’une mission de secours. Comme celui le dira lui-même dans l’adaptation cinématographique (Scott 2015) :

> “In the face of overwhelming odds, I’m left with only one option, I’m gonna have to **science** the shit out of this.” (Mark Watney)

Pour se sortir de sa fâcheuse situation, le protagoniste n’aura comme outils que la science et ses connaissances techniques.

L’histoire se situe dans un avenir proche de nous, le film situe l’action en 2035. Elle compte peu de _novums_ si ce n’est une agronomie de survie (nous n’avons pas encore planté des pommes de terre sur Mars mais ce n’est pas irréaliste (Shamsian 2015-/1) ) et la navette Hermès qui n’existe pas (mais utilise un moteur ionique similaire à celui utilisé par plusieurs sondes actuelles). Les technologie utilisées sont celles de la technologie spatiale que nous connaissons. La production d’eau d’irrigation à partir de carburant pour fusée, bien que périlleuse, est réalisable avec la chimie actuelle. La mission de secours utilisera l’assistance gravitationnelle pour atteindre plus vite Mars, technique utilisée pour les sondes Voyager 2 ou Cassini-Huygens.

Le survivant rétablira la communication avec les responsables de la mission sur Terre de plusieurs manières. Notamment en faisant appel aux ressources locales comme la sonde Pathfinder et le robot Sojourner des premiers temps de l’exploration martienne. Les temps de trajet des ondes radio ont été calculés en fonction des distances séparant les protagonistes pour ajouter du réalisme. Ce frein à la communication est un des ressorts scénaristiques principaux, puisqu’il y aura toujours un décalage entre les évènements sur Mars et leur perception sur Terre.

Initialement, le roman a été rédigé sous forme de feuilleton sur le site web de l’auteur. Des courriels d’astronautes, personnels de la NASA ou scientifiques (géologues, chimistes, physiciens) ont permit le relevé d’erreurs, permettant leur correction avant la parution de l’ouvrage (Weir 2014a). En essayant de rester au plus près des connaissances actuelles, Weir est plus proche du merveilleux scientifique de Verne que de la Science Fiction moderne. Il est, à l’instar de Verne, un “vulgarisateur passionné de sciences” (Vas-Deyres 2017) et Mark Watney à la fois docteur en biologie et pionnier martien, un “savanturier”.

# Bibliographie

Al von Ruff and the ISFDB team. n.d. “ISFDB: The Martian.” The Internet Speculative Fiction Database. Accessed May 24, 2020. [http://www.isfdb.org/cgi-bin/title.cgi?1678924](http://www.isfdb.org/cgi-bin/title.cgi?1678924).

Scott, Ridley, dir. 2015. _The Martian_. Adventure, Drama, Sci-Fi. Twentieth Century Fox, TSG Entertainment, Scott Free Productions.

Shamsian, Jacob. 2015-/1. “Fact-Checking ‘the Martian’: Can You Really Grow Plants on Mars? - Modern Farmer.” Modern Farmer. 2015-/1. [https://modernfarmer.com/2015/10/can-you-grow-plants-on-mars/](https://modernfarmer.com/2015/10/can-you-grow-plants-on-mars/).

Vas-Deyres, Natacha. 2017. “De La Tradition Utopique à Jules Verne : Fondations de L’imaginaire Science-Fictionnel En France.” MOOC Science-Fiction.

Weir, Andy. 2014a. “How Science Made Me a Writer.” Salon. February 12, 2014. [https://www.salon.com/2014/02/11/how\_science\_made\_me\_a_writer/](https://www.salon.com/2014/02/11/how_science_made_me_a_writer/).

———. 2014b. _The Martian_. Reprint edition. New York: Broadway Books.

———. 2015\. “About Andy Weir.” _Andy Weir_ (blog). October 2, 2015. [https://www.andyweirauthor.com/bios/andy-weir](https://www.andyweirauthor.com/bios/andy-weir).
