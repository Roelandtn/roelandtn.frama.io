---
title: "About"
date: "2021"
---

> "I do open source because it this only right way to do software. But at the same time, I'd use the best tool for the job [...]." - Linus Torvalds on Git [^linus on git]

This website is the personnal website of Nicolas Roelandt. He is a former land surveyor, got a master degree in GIS in 2018.

He likes Python and starting to like R more every day. He is involved in several open sources projects like [*OSGeo live*](https://live.osgeo.org/en/index.html) and he is member of the [*OSGeo French Chapter*](http://www.osgeo.asso.fr/). As an active member, he participated to the organization of the several [**FOSS4G-fr**](http://osgeo.asso.fr/foss4gfr-2016/) and [**FOSS4G-Eu 2017**](https://europe.foss4g.org/2017/).

So his posts will be mostly talking about GIS and free software related subjects (like PostgreSQL/PostGIS, Docker, Ubuntu...).

You can find him on:

- Github: [Bakaniko](https://github.com/Bakaniko)
- LinkedIn: [nicolasroelandt](https://www.linkedin.com/in/nicolasroelandt/)
- Twitter: [RoelandtN42](https://twitter.com/RoelandtN42)

All documents on this site are licenced in CC BY-SA.

Please note that english is not the primary language of the author and mistakes and omissions can be made. So please be nice and signal them to the author.


This website is build with the [**blogdown**](https://github.com/rstudio/blogdown) package. The theme "Future Imperfect" was forked from the version of *Patrick Collins* [@pacollins/hugo-future-imperfect](https://github.com/pacollins/hugo-future-imperfect) and modified to work with gitlab-ci.

[^linus on git]: [Tech Talk: Linus Torvalds on git : https://youtu.be/4XpnKHJAok8?t=9m45s](https://youtu.be/4XpnKHJAok8?t=9m45s)

