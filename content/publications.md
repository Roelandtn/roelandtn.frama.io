---
title: Publications
author: Nicolas
date: '2023-12-06'
slug: publications
categories:
  - Writing
  - Projects
  - blogging
  - teaching
tags:
  - blog
description: ''
featured: ''
featuredalt: ''
featuredpath: ''
linktitle: ''
type: post
---


This page gathers ressources I published.

- [2019-2023 Cours FOSS4G](https://roelandtn.frama.io/cours-foss4g/) : course handout on OSGeo and Free and Libre Software for Geomatic (FOSS4G), in French.

- 2019-2023 Introduction à PyQGIS (3 days course to get hands on PyQGIS and QGIS plugin development, in French)
  - [Carthagéo Numérique](https://roelandtn.frama.io/cours_pyqgis/) 
  - [Carthagéo Thématique](https://roelandtn.frama.io/pyqgis-thematique/)
- [2019-2020 Introduction to geospatial data with R](https://roelandtn.frama.io/cours_r_pdm/) : 9 hours course on to handling geodata in R, from scratch, in English.

- [2019-06-28 Meetup Raddict on geospatial data in R](https://roelandtn.frama.io/slides/2090628_meetup_Raddict_datageo.html), slides in  French.

- [2019 SIG – Introduction à la géomatique et mise en place d’un système d’information géographique libre](https://www.d-booker.fr/solutions-informatiques/582-sig-libre.html), book about GIS and creating a FOSS4G Spatial Data Infrastructure
