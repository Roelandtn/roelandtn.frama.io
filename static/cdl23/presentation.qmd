---
title: "Au-delà de Google maps, l'écosystème de la géomatique libre"
author: "Nicolas Roelandt"
affiliation: 
  - Association OSGeo-fr
  - Univ. Gustave Eiffel
format:
  revealjs:
    incremental: true
    logo: fichiers/logos.png
    footer: "Au-delà de Google maps, l'écosystème de la géomatique libre - Nicolas Roelandt - #CdL2023"
bibliography: "fichiers/bibliographie.bib"
---

## Qui suis-je ?

-   Géomaticien à l'[Université Gustave Eiffel](https://www.univ-gustave-eiffel.fr/), le jour.
-   Membre de l'association [OSGeo-fr](https://www.osgeo.fr/), la nuit et le week-end.

## L'OSGeo-fr

::: columns
::: {.column width="60%"}
-   Chapitre local de la fondation [OSGeo](https://www.osgeo.org/)
-   promotion du géospatial libre:
    -   logiciels
    -   données
    -   standards
    -   éducation
-   [www.osgeo.fr](https://www.osgeo.fr/)
:::

::: {.column width="40%"}
[![](fichiers/logo-osgeofr.png){fig-alt="Logo en camaieu de vert de l'association OSGeo-fr" fig-align="center"}](https://www.osgeo.asso.fr/)
:::
:::

## Parlons de l'objet **Carte**

-   Google maps est une carte (**c'est dans le nom**)
-   document graphique
-   représente un espace géographique
-   vecteur d'information
-   doit être lisible **facilement** et **rapidement**

## Cartographier, c'est faire des choix

::: columns
::: {.column width="40%"}
[![](fichiers/Manifeste_cartographes.png){fig-alt="Manifeste des cartographes en 10 articles, écrit en blanc sur fond orange" fig-align="center"}](https://neocarto.hypotheses.org/5402)
:::

::: {.column width="60%"}
- Simplifier
- Sélectionner
- Hiérarchiser
:::
:::

## Blanc des cartes

-   information non portée sur la carte.
    -   par manque d'information
    -   par choix
-   la carte est un objet politique 
[![](fichiers/300px-Do-not-edit.png)](https://wiki.openstreetmap.org/wiki/FR:Russian-Ukrainian_war)

## Blanc des cartes

#### Travaux de Matthieu Noucher (Géographe - UMR Passages)

::: smaller
Ouvrage [Blancs des cartes et boîtes noires algorithmiques](https://www.cnrseditions.fr/catalogue/geographie-territoires/blancs-des-cartes-et-boites-noires-algorithmiques/) aux éditions CNRS

Interview par la [librairie Mollat - 2023-06-30](https://yewtu.be/watch?v=uRbkwGC7zss)
:::

## Déchetterie de Briis-sous-Forge (91)

Exemple tiré du billet de blog [Cipher Bliss](https://www.cipherbliss.com/comparaison-entre-openstreetmap-et-googlemap-googleknowsnothing/)

[![Orthophoto Copyright IGN](fichiers/ign_ortho_briis.png){fig-alt="Image aérienne de la déchetterie de Briis-sous-forge" fig-align="center"}](https://www.geoportail.gouv.fr/carte?c=2.1280350565507535,48.616011243203246&z=18&l0=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.MAPS.BDUNI.J1::GEOPORTAIL:OGC:WMTS(1;h)&l2=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1;h)&permalink=yes)

## Briis-sous-forge (Google Maps)

[![Déchetterie de Briis-sous-Forge (Cipher Bliss 2019)](fichiers/screenshot-www.google.com-2019.07.25-10-11-46-620x685.png){fig-alt="Capture d'écran de Google maps sur le site de la déchetterie de Briis-sous-Forge" fig-align="center"}](https://www.cipherbliss.com/comparaison-entre-openstreetmap-et-googlemap-googleknowsnothing/)

## Briis-sous-forge (OSM)

[![Capture d'écran d'OpenStreetMap (Cypher Bliss 2019)](fichiers/carto_osm_briis-620x632.png){fig-alt="Capture d'écran d'OpenStreetMap centrée sur la déchetterie de Briis-sous-Forge" fig-align="center"}](https://www.cipherbliss.com/comparaison-entre-openstreetmap-et-googlemap-googleknowsnothing/)

## Briis-sous-forge (IGN Plan)

[![Geoportail IGN (couche PLAN)](fichiers/ign_plan_briis.png){fig-alt="Capture d'écran du Geoportail IGN (couche PLAN) de la déchetterie de Briis-sous-Forge" fig-align="center"}](https://www.geoportail.gouv.fr/carte?c=2.1280350565507535,48.616011243203246&z=18&l0=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.MAPS.BDUNI.J1::GEOPORTAIL:OGC:WMTS(1;h)&l2=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&permalink=yes)

## Briis-sous-forge (IGN J+1)

[![Geoportail IGN (couche PLAN J+1)](fichiers/ign_planj1_briis.png)](https://www.geoportail.gouv.fr/carte?c=2.1280350565507535,48.616011243203246&z=18&l0=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1;h)&l1=GEOGRAPHICALGRIDSYSTEMS.MAPS.BDUNI.J1::GEOPORTAIL:OGC:WMTS(1)&l2=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1;h)&permalink=yes)

## Comment éviter le blanc des cartes ?

::: columns
::: {.column width="50%" .nonincremental}
-   Pas possible sauf éditeur ou auteur

-   Initiative [HOT (Humanitarian OpenStreetmap Team)](https://www.hotosm.org/)

-   Association [CartONG](https://www.cartong.org/fr)
:::

::: {.column width="50%"}
[![Source : hotosm.org](fichiers/hot-logo-text.svg){fig-alt="Logo de l'Humanitarian Openstreetmap Team"}](https://www.hotosm.org/)

[![Source : cartong.org](fichiers/CartONG_logo_long_vector.svg){fig-alt="Logo de l'association CartONG"}](https://www.cartong.org/fr)
:::
:::
## D'où vient l'information présente sur Google Maps ?

-   Google cars
-   Données publiques et/ou payantes
-   Professionnels
-   Des utilisateurs

## La question des données personnelles

-   cookies et trackers

-   position géographique et temporalité

-   utilisation ?

-   revente ?

## Localisation et temporalité {.smaller}

::: columns
::: {.column width="50%"}
> Pour Dominique Trinquand, Général à la retraite, l'utilisation de cette application \[Stava\] fait courir un danger à ces soldats connectés en terrain de guerre: "On peut repérer par ce système là, même si c'est à l'intérieur de la base, l'heure à laquelle les soldats courent habituellement. Ca peut permettre un ciblage, un tir de mortier, une bombe qui a été posée et qui va exploser, donc c'est extrêmement dangereux". source : [francetvinfo.fr](https://www.francetvinfo.fr/economie/emploi/metiers/armee-et-securite/video-strava-l-application-de-running-qui-devoile-les-positions-de-militaires-francais_2586682.html)
:::

::: {.column width="50%"}
[![Source : Kulture Geek.fr](fichiers/Strava-Localisation-Base-Militaire-Niger.jpg){fig-alt="Capture d'écran de l'application Strava, on distingue bien les contours de la base où courent les militaires"}](https://kulturegeek.fr/news-131444/lapplication-sportive-strava-devoile-erreur-localisation-plusieurs-bases-militaires-secretes)
:::
:::

# Quelles alternatives ?

Décomposition en services

## Calculs d'itinéraires

::: columns
::: {.column width="50%"}
-   OpenStreetMap
-   OSMAnd
-   Organic Maps
:::

::: {.column width="50%"}
[![Source : osmand.net/](fichiers/OSMANd.png){fig-alt="Capture d'écran du site osmand.net, on voit terminaux android avec des fonds de carte différents"}](https://osmand.net/)
:::
:::

## Navigation

-   OsmAnd
-   Organic Maps

. . .

[![Source : organicmaps.app](fichiers/organic_maps.png){fig-alt="Capture d'écran du site organicmaps.app, "}](https://organicmaps.app/fr/)

## Streetview

-   Panoramax
    -   GéoCommun porté par l'IGN et OSM France

. . .

[![Source : panoramax.fr](fichiers/panoramax.png){fig-alt="Capture d'écran du site Panoramax.fr, on voit une carte de la France avec des traces oranges et un encart avec une photo 3D d'une route de campagne"}](https://panoramax.fr/photos#focus=map&map=4.5/47/3&speed=250)

## Streetview

-   Panoramax
    -   GéoCommun porté par l'IGN et OSM France
-   Mapillary

## Afficher des points d'intérêts sur une page web

-   Leaflet + fond de carte OpenStreetMap
-   Umap

. . . 

[![Source : umap.openstreetmap.fr](fichiers/umap.png){fig-alt="Capture d'écran du site umap.openstreetmap.fr, plusieurs exemples de cartes réalisées avec l'outil"}](https://umap.openstreetmap.fr/fr/)

## Cartographie d'édition

-   [QGIS](https://www.qgis.org/)

. . .

![Source : Nicolas Roelandt](fichiers/qgis.png){fig-alt="Capture d'écran de l'outil QGIS montrant l'analyse d'un incendie de forêt à l'aide d'images satellite"}

## Conclusion

[roelandtn.frama.io/cours-foss4g/](https://roelandtn.frama.io/cours-foss4g/)

##  {.center}

### Au-delà de Google maps, l'écosystème de la géomatique libre

Nicolas Roelandt [\@NRoelandt\@social.sciences.re](https://social.sciences.re/@NRoelandt)

  
![](fichiers/logos.png)

[www.osgeo.fr](https://www.osgeo.fr/)

---
nocite: |
  @*
---

## 

### Bibliographie {.smaller}
