## importation des bibliothèques nécessaires
import processing
from qgis.core import QgsProject

# Chargement des données
## arrondissements
uri = "/home/niko/Téléchargements/lyon/arrondissements.shp"
arrondissements = iface.addVectorLayer(uri,"arrondissements", "ogr")

## gares
uri = "/home/niko/Téléchargements/lyon/gares_sncf.shp"
gares = iface.addVectorLayer(uri,"gares", "ogr")

## métros
uri = "/home/niko/Téléchargements/lyon/entrees_sorties_metro.shp"
metro = iface.addVectorLayer(uri,"entrees_sorties_metro", "ogr")

## Contrôle des projections

for layer in QgsProject.instance().mapLayers().values():
    crs = layer.crs().authid()
    print("{} : {}".format(layer.name(), crs))


## gares sncf n'est pas dans la même projection,
# il faut la reprojeter
gares_reproject = processing.run(
    "native:reprojectlayer", 
    {'INPUT':gares,
    'TARGET_CRS':QgsCoordinateReferenceSystem('EPSG:2154'),
    'OUTPUT':'memory:'})['OUTPUT']

## Contrôle de l'opération
for layer in QgsProject.instance().mapLayers().values():
    crs = layer.crs().authid()
    print("{} : {}".format(layer.name(), crs))

# Extraction de la gare de Part-Dieu
expression = "nom = 'Gare de Lyon Part-Dieu'"
partdieu = processing.run("native:extractbyexpression",
{'INPUT':gares_reproject, 'EXPRESSION': expression,'OUTPUT':'memory'})['OUTPUT']

# Création d'un tampon de 500m
buffer_partdieu = processing.runAndLoadResults(
    "native:buffer", 
    {'INPUT':partdieu,'DISTANCE':1000,'SEGMENTS':5,'END_CAP_STYLE':0,'JOIN_STYLE':0,'MITER_LIMIT':2,'DISSOLVE':False,'OUTPUT':'memory:buffer_partdieu'})['OUTPUT']
    
# Intersection des stations de métros
metro_1000m = processing.runAndLoadResults(
    "native:intersection", 
    {'INPUT':metro,'OVERLAY':buffer_partdieu,'INPUT_FIELDS':[],'OVERLAY_FIELDS':[],'OUTPUT':'memory:metro_1000m'})['OUTPUT']

# Renommer la couche intersectée
## identifier la couche
project =  QgsProject.instance()
print(project.mapLayers())
for id, layer in project.mapLayers().items():
        print(layer.name())

## renommer
project.mapLayersByName('Intersection')[0].setName("metro_1000m")

## sauvegarder
layer = project.mapLayersByName('metro_1000m')[0]
output_path="/home/niko/Téléchargements/lyon/metro1000m.gpkg"
QgsVectorLayerExporter.exportLayer(layer = layer, uri = output_path, providerKey = 'ogr', destCRS = QgsCoordinateReferenceSystem('EPSG:2154'))